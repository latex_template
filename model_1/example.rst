.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


.. role:: latex(raw)
   :format: latex

.. role:: level
.. role:: star

.. default-role:: latex

.. raw:: latex


  \setstretch{1.4}

  % Espacement entre chaque note de marge
  \setlength{\notesep}{1.3cm}

  \renewcommand{\labelitemi}{}


.. role:: smallcaps
.. role:: smallemph

.. |tuxicon|      replace::   `\renewcommand{\labelitemi}{\faLinux}`
.. |phoneicon|    replace::   `\renewcommand{\labelitemi}{\faPhone}`
.. |mailicon|     replace::   `\renewcommand{\labelitemi}{\faEnvelope}`
.. |puzzle|       replace::   `\renewcommand{\labelitemi}{\faPuzzlePiece}`
.. |noicon|       replace::   `\renewcommand{\labelitemi}{}`
.. |checkicon|    replace::   `\renewcommand{\labelitemi}{\faCheck}`
.. |coffeeicon|   replace::   `\renewcommand{\labelitemi}{\faCoffee}`
.. |lambdaicon|   replace::   `\renewcommand{\labelitemi}{$\lambda$}`

=============
Martin Durand
=============

------------------
Créateur de talent
------------------

.. sidebar:: Informations

  * 17 allée des cerisiers
  * 75001 Paris |phoneicon|
  * 01 23 45 67 89 |mailicon|
  * email@example |noicon|

.. sidebar:: Compétences

  .. class:: checklist

  * Conception
  * Esprit de synthèse
  * Écoute |noicon|

.. sidebar:: Niveaux

  Étoiles

  :star:`4.5`

  Autres

  :level:`5`


Expériences
===========

`\renewcommand{\labelitemi}{$\bullet$}`

:Actuellement:

  **Poste actuel** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][4-7]`

  - :latex:`\lipsum[1-3][1]`
  - :latex:`\lipsum[1-3][2]`


Réalisations
============

Description
-----------

:latex:`\lipsum[4-7][10-15]`

Description
-----------

:latex:`\lipsum[7-10][10-15]`


Formations
==========

:Année:

    **Niveau M1** — *Université de Paris*


:Année:

    **Licence** — *Université de Paris*

    Mention


:Année:

    **Baccalauréat Scientifique** — *Lycée* – Ville

    Spécialité
