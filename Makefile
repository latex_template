SUBDIRS := $(wildcard model*/. example*/.)

all: $(SUBDIRS)

$(SUBDIRS):
	$(MAKE) -j -C $@

pngs: $(SUBDIRS)
	for dir in $(SUBDIRS); do \
		gs -dTextAlphaBits=4 -dFirstPage=1 -dLastPage=1 -dBATCH -dNOPAUSE -sDEVICE=png16m -r72 -dUseCropBox -sOutputFile=$$dir/example.png  $$dir/example.pdf; \
	done

clean:
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir clean; \
	done


.PHONY: all $(SUBDIRS)

