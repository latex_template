.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


==========
Main title
==========

.. sectnum::
  :depth: 2

First Title
-----------

ipsum
~~~~~

.. sidebar:: Note

    .. raw:: latex

       \lipsum[3-9][10-11]

.. raw:: latex

   \lipsum[1-9][5-10]

.. code:: c

    #include <stdio.h>
    int main() {
       printf("Hello, World!");
       return 0;
    }

Last one
--------

.. list-table::
   :header-rows: 1

   * - Treat
     - Quantity
     - Description
   * - Albatross
     - 2.99
     - On a stick!
   * - Crunchy Frog
     - 1.49
     - If we took the bones out, it wouldn't be
       crunchy, now would it?
   * - Gannet Ripple
     - 1.99
     - On a stick!
