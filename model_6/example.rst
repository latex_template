.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


.. role:: latex(raw)
   :format: latex

.. role:: level
.. role:: levelbox
.. role:: star

.. default-role:: latex


.. role:: smallcaps
.. role:: smallemph

.. |phoneicon|    replace::   `\renewcommand{\labelitemi}{\faPhone}`
.. |mailicon|     replace::   `\renewcommand{\labelitemi}{\faEnvelope}`
.. |noicon|       replace::   `\renewcommand{\labelitemi}{}`

=============
Nom et Prénom
=============

--------------
Petite légende
--------------

:latex:`\renewcommand{\labelitemi}{}`


.. container:: header

  * 17 allée des cerisiers
  * 75001 Paris |phoneicon|
  * 01 23 45 67 89 |mailicon|
  * email@example |noicon|
  * 01/02/1980
  * Permis B

Expériences
===========

`\renewcommand{\labelitemi}{$\bullet$}`

:Actuellement:

  **Poste actuel** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][4-7]`

  .. class:: checklist

    - :latex:`\lipsum[1-3][1]`
    - :latex:`\lipsum[1-3][2]`
    - :latex:`\lipsum[1-3][3]`

:2011 — 2018:

  **Poste occupé** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][5-10]`

  :latex:`\lipsum[4-7][5-10]`

:2010 — 2011:

  **Poste occupé** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][10-15]`


:2008 — 2010:

  **Poste occupé** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][15-21]`


Réalisations
============

Description
-----------

:latex:`\lipsum[4-7][10-15]`

:latex:`\lipsum[4-7][15-20]`

Description
-----------

:latex:`\lipsum[7-10][10-15]`

:latex:`\lipsum[7-10][15-20]`


Formations
==========

:Année:

    **Niveau M1** — *Université de Paris*


:Année:

    **Licence** — *Université de Paris*

    Mention


:Année:

    **Baccalauréat Scientifique** — *Lycée* – Ville

    Spécialité
