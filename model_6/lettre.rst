.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


.. role:: latex(raw)
   :format: latex

.. default-role:: latex

:latex:`\renewcommand{\labelitemi}{}`

.. |phoneicon|    replace::   `\renewcommand{\labelitemi}{\faPhone}`
.. |mailicon|     replace::   `\renewcommand{\labelitemi}{\faEnvelope}`
.. |noicon|       replace::   `\renewcommand{\labelitemi}{}`

.. container:: header

  * 17 allée des cerisiers
  * 75001 Paris |phoneicon|
  * 01 23 45 67 89 |mailicon|
  * email@example |noicon|

* Company Recruitment team
* *Company, Inc.*
* *123 somestreet*
* *some city*

.. raw:: latex

    \setstretch{1.5}
    \vspace{2cm}

:latex:`\lipsum[1-3]`

