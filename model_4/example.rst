.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


.. role:: latex(raw)
   :format: latex

.. role:: level
.. role:: star

.. default-role:: latex

.. raw:: latex


  \setstretch{1.4}
  %\definecolor{orangeColor}{RGB}{255, 165, 0}

  % Espacement entre chaque note de marge
  \setlength{\notesep}{1.3cm}

  \renewcommand{\labelitemi}{}


.. role:: smallcaps
.. role:: smallemph

.. |EDD|          replace::   :smallcaps:`EDD`
.. |ISO9001|      replace::   :smallcaps:`ISO9001`
.. |PDF|          replace::   :smallcaps:`PDF`

.. |ITIL|         replace::   :smallcaps:`ITIL`

.. |tuxicon|      replace::   `\renewcommand{\labelitemi}{\faLinux}`
.. |phoneicon|    replace::   `\renewcommand{\labelitemi}{\faPhone}`
.. |mailicon|     replace::   `\renewcommand{\labelitemi}{\faEnvelope}`
.. |puzzle|       replace::   `\renewcommand{\labelitemi}{\faPuzzlePiece}`
.. |noicon|       replace::   `\renewcommand{\labelitemi}{}`
.. |checkicon|    replace::   `\renewcommand{\labelitemi}{\faCheck}`
.. |coffeeicon|   replace::   `\renewcommand{\labelitemi}{\faCoffee}`
.. |lambdaicon|   replace::   `\renewcommand{\labelitemi}{$\lambda$}`

=============
Martin Durand
=============

------------------
Créateur de talent
------------------

.. sidebar:: Informations

  * 17 allée des cerisiers
  * 75001 Paris |phoneicon|
  * 01 23 45 67 89 |mailicon|
  * email@example |noicon|
  * 01/02/1980
  * Permis B

.. sidebar:: Compétences

  .. class:: checklist

  * Conception
  * Esprit de synthèse
  * Écoute |noicon|
  * \

.. sidebar:: Niveaux

  Autre

  :star:`4.5`

  Anglais

  :level:`5`

  Bureautique

  :level:`3`



Expériences
===========

`\renewcommand{\labelitemi}{$\bullet$}`

:Actuellement:

  **Poste actuel** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][4-7]`

  - :latex:`\lipsum[1-3][1]`
  - :latex:`\lipsum[1-3][2]`
  - :latex:`\lipsum[1-3][3]`


:2011 — 2018:

  **Poste occupé** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][5-10]`

  :latex:`\lipsum[4-7][5-10]`

:2010 — 2011:

  **Poste occupé** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][10-15]`


:2008 — 2010:

  **Poste occupé** — *Entreprise* — Adresse.

  :latex:`\lipsum[1-3][15-21]`


Réalisations
============

Description
-----------

:latex:`\lipsum[4-7][10-15]`

:latex:`\lipsum[4-7][15-20]`

Description
-----------

:latex:`\lipsum[7-10][10-15]`

:latex:`\lipsum[7-10][15-20]`


Formations
==========

:Année:

    **Niveau M1** — *Université de Paris*


:Année:

    **Licence** — *Université de Paris*

    Mention


:Année:

    **Baccalauréat Scientifique** — *Lycée* – Ville

    Spécialité

.. raw:: latex

  \vspace{1cm}
  \center\pgfornament[color=backsidecolor,width=1cm]{3}
