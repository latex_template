.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


.. role:: latex(raw)
   :format: latex

.. default-role:: latex


.. raw:: latex

  % Création d'un nouvel environnement pour créer une boite colorée
  \NewEnviron{DUCLASScolorbox}[1]{
	\fcolorbox{#1}{#1}{\BODY}
  }

  \setstretch{2.4}

Titre
=====


`\switchcolumn`

.. container:: colorbox-olive textcolor-white hbox-10cm centering

  `\lipsum[3][1-2]`

.. container:: colorbox-purple textcolor-white hbox-15cm centering

  `\lipsum[3][3-5]`

`\switchcolumn`


Section
-------

`\lipsum[1]`

