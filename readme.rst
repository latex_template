.. default-role:: literal

Différents modèles avec rst & latex

Clone et installation
=====================

.. code-block:: bash

  git clone http://git.chimrod.com/latex_template.git/


Pré-requis
----------

La compilation des fichiers en PDF requiert python et docutils. Il faut bien
sûr latex pour générer le pdf.

Les dépendances nécessaire peuvent être installées avec la commande suivante :

.. code-block:: bash

  sudo apt install git make docutils texlive texlive-latex-extra \
    texlive-lang-french texlive-fonts-extra texlive-lualatex

compilation
-----------

Tous les modèles peuvent être générés en une seule commande en lançant `make` dans le répertoire principal.

Organisation
============

Chaque répertoire contient un style de document. La commande `make` permet de
générer le fichier PDF à partir du fichier source.

Pour réutiliser un modèle, il suffit de créer ces lignes dans un fichier
`Makefile` :

.. code:: Makefile

  MODEL = ../model_7 # Chemin vers le modèle à utiliser
  include $(MODEL)/../common/Makefile.common

et de créer un document texte avec l’extension `.rst`. La commande `make` va
lancer la compilation du fichier en pdf.

Personalisation
---------------

Les règles de basent peuvent être personalisées en fonction des besoins.

Chaque fichier de style présent dans le répertoire du modèle peut être
surchargé par un fichier de style de même nom présent dans le répertoire
courant du modèle.

:`EXTRA_RST_OPTIONS`:

    Cette option va donner des directives supplémentaires au compilateur
    rst2latex

    Exemple :

    .. code:: Makefile

        EXTRA_RST_OPTIONS = \
        		--table-style=booktabs


:`EXTRA_STYLES`:

    Cette option permet d’ajouter des styles supplémentaires qui sont appliqués
    sur le document généré.

    .. code:: Makefile

        EXTRA_STYLES = fontsize\

:`FILTERS`:

    Cette option permet de lister les modules présents dans le modèle de base
    et ne devant pas être chargé. (En vue de les surcharger par exemple).

    .. code:: Makefile

        FILTERS = %10_geometry.tex \
        		  %10_title_style.tex \
        		  %00_colors.tex \

Exemples
========

Les images ci-dessous sont construites à partir des fichiers exemples :

Model 1
-------

.. image:: /latex_template.git/plain/snapshots/model1.png
  :width: 400px

`[Version PDF]`__

.. __: /latex_template.git/plain/model_1/example.pdf


Model 2
-------

Le bandeau du titre occupe la totalité de la largeur :

.. image:: /latex_template.git/plain/snapshots/model2.png
  :width: 400px

`[Version PDF]`__

.. __: /latex_template.git/plain/model_2/example.pdf

Model 3
-------

Ce modèle inverse la colonne en la faisant passer à droite :

.. image:: /latex_template.git/plain/snapshots/model3.png
  :width: 400px

`[Version PDF]`__

.. __: /latex_template.git/plain/model_3/example.pdf

Model 5
-------

Une version inspirée par le `CV Rambo`__

.. __: https://gitlab.com/benoit.vaillant/tscvng/-/tree/master/CVs/rambo

.. image:: /latex_template.git/plain/snapshots/model5.png
  :width: 400px

`[Version PDF]`__

.. __: /latex_template.git/plain/model_5/example.pdf

Model 6
-------

Une version inspirée par le fameux `moderncv`__

.. __: https://ctan.org/pkg/moderncv

.. image:: /latex_template.git/plain/snapshots/model6.png
  :width: 400px

`[Version PDF]`__

.. __: /latex_template.git/plain/model_6/example.pdf

Model 7
-------

Modèle utilisé pour un mariage

.. image:: /latex_template.git/plain/snapshots/model7.png
  :width: 400px

Commandes fournies par ce modèle :

:pageornament:

    Commande appliquée en fond de page sur chaque page. Elle prend trois
    arguments :

    - La dimension de l’image
    - La distance par rapport au bord de l’image
    - La couleur à appliquer

:altColor:

    la couleur `altColor` est utilisée pour le titre principal du document, les
    décoration de page, ainsi que les lignes encadrant les titres de chapitre.

    Valeur par défaut `lightgray`

Model 8
-------

.. image:: /latex_template.git/plain/snapshots/model8.png
  :width: 400px

La syntaxe
==========

La colonne latérale
-------------------

Le code suivant permet de créer une section dans la colonne :


.. code-block:: rst

  .. sidebar:: Informations

    * 17 allée des cerisiers
    * 75001 Paris
    * 01 23 45 67 89
    * email@example
    * 01/02/1980
    * Permis B


Les graphiques
--------------

La ligne suivante introduit les étoiles colorées en fonction du niveau (jusque
5)

.. code-block:: rst

  :star:`4.5`

La commande `level` l'affiche sous la forme d'une petite barre :

.. code-block:: rst

  :level:`3`
