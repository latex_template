.. -*- mode: rst -*-
.. -*-  coding: utf-8 -*-


Main title
==========

.. raw:: latex

 \setstretch{1.2}
 \fontsize{13pt}{15pt}\selectfont

Title
-----

.. raw:: latex

 \lipsum[1][1-5]
 \begin{multicols}{2}

1
-

.. raw:: latex

   \lipsum[1-9][5-15]

2
-

.. raw:: latex

   \lipsum[2-3]


.. raw:: latex

 \end{multicols}

